package com.ahantiev.satelite.common.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.ConstraintViolation;
import javax.validation.Valid;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FlightProgram {
    @NotNull
    private Long startUp;
    @Valid
    @NotEmpty
    @Builder.Default
    private List<Operation> operations = Collections.emptyList();

    public static FlightProgram.FlightProgramBuilder ofDefault(Operation... operation) {
        return builder().startUp(0L).operations(Arrays.asList(operation));
    }

    public Map<String, String> validate() {
        Map<String, String> errors = new HashMap<>();

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Set<ConstraintViolation<FlightProgram>> constraints = factory.getValidator().validate(this);

        if (!constraints.isEmpty()) {
            for (ConstraintViolation<FlightProgram> constraint : constraints) {
                errors.put(constraint.getPropertyPath().toString(), constraint.getMessage());
            }
        }

        validateUniqueOperation(errors);
        validateSupportedVariable(errors);

        return errors;
    }

    private void validateSupportedVariable(Map<String, String> errors) {
        for (int i = 0; i < this.getOperations().size(); i++) {
            Operation operation = this.getOperations().get(i);
            if (Objects.isNull(operation.getVariable()) || Settings.supportedParams.contains(operation.getVariable())) {
                continue;
            }

            errors.put(String.format("operations[%d].variable", i), "not supported param");
        }
    }

    private void validateUniqueOperation(Map<String, String> errors) {
        Set<Operation> uniqueOperations = Set.copyOf(this.getOperations());

        if (uniqueOperations.size() != this.getOperations().size()) {
            errors.put("operation.id", "must be unique");
        }
    }
}
