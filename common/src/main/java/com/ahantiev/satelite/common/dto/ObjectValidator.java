package com.ahantiev.satelite.common.dto;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ObjectValidator {

    private static ValidatorFactory factory = Validation.buildDefaultValidatorFactory();

    public static Map<String, String> validate(Object o) {
        Map<String, String> errors = new HashMap<>();

        Set<ConstraintViolation<Object>> constraints = factory.getValidator().validate(o);

        if (!constraints.isEmpty()) {
            for (ConstraintViolation<?> constraint : constraints) {
                errors.put(constraint.getPropertyPath().toString(), constraint.getMessage());
            }
        }

        return errors;
    }
}
