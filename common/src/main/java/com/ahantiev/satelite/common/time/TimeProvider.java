package com.ahantiev.satelite.common.time;

import java.time.*;

public class TimeProvider {
    private static Clock clock = Clock.systemDefaultZone();
    private static ZoneId zoneId = ZoneId.systemDefault();

    public static Instant now() {
        return Instant.now(clock);
    }

    public static void useFixedClockAt(Instant instant) {
        clock = Clock.fixed(instant, zoneId);
    }
}