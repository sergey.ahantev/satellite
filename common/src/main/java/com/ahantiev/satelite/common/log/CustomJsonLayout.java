package com.ahantiev.satelite.common.log;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.contrib.json.classic.JsonLayout;

import java.time.Instant;
import java.util.LinkedHashMap;
import java.util.Map;

public class CustomJsonLayout extends JsonLayout {
    public static final String MDC_PARAM_TYPE = "type";
    public static final String TIME = "time";

    @Override
    protected Map toJsonMap(ILoggingEvent event) {

        Map<String, Object> map = new LinkedHashMap<String, Object>();
        if (event.getLevel().equals(Level.ERROR)) {
            long epochSecond = Instant.ofEpochMilli(event.getTimeStamp()).getEpochSecond();
            add(MDC_PARAM_TYPE, true, event.getMDCPropertyMap().get(MDC_PARAM_TYPE), map);
            add(TIMESTAMP_ATTR_NAME, this.includeTimestamp, String.valueOf(epochSecond), map);
            add(FORMATTED_MESSAGE_ATTR_NAME, this.includeFormattedMessage, event.getFormattedMessage(), map);
            addThrowableInfo(EXCEPTION_ATTR_NAME, this.includeException, event, map);
            addCustomDataToJsonMap(map, event);
        } else {
            addTimestamp(TIME, this.includeTimestamp, event.getTimeStamp(), map);
            add(LEVEL_ATTR_NAME, this.includeLevel, String.valueOf(event.getLevel()), map);
            add(FORMATTED_MESSAGE_ATTR_NAME, this.includeFormattedMessage, event.getFormattedMessage(), map);
            addThrowableInfo(EXCEPTION_ATTR_NAME, this.includeException, event, map);
            addCustomDataToJsonMap(map, event);
        }

        return map;
    }
}
