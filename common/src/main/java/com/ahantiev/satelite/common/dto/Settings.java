package com.ahantiev.satelite.common.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Settings {
    public static final String VALIDATION_NOT_SUPPORTED_PARAMS = "notSupportedParams";

    public static final String RADIO_POWER_DBM = "radioPowerDbm";
    public static final String COOLING_SYSTEM_POWER_PCT = "coolingSystemPowerPct";
    public static final String MAIN_ENGINE_THRUST_PCT = "mainEngineThrustPct";
    public static final String ORIENTATION_ZENITH_ANGLE_DEG = "orientationZenithAngleDeg";
    public static final String ORIENTATION_AZIMUTH_ANGLE_DEG = "orientationAzimuthAngleDeg";
    public static final String VESSEL_ALTITUDE_M = "vesselAltitudeM";
    public static final String VESSEL_SPEED_MPS = "vesselSpeedMps";
    public static final String MAIN_ENGINE_FUEL_PCT = "mainEngineFuelPct";
    public static final String TEMPERATURE_INTERNAL_DEG = "temperatureInternalDeg";

    public static List<String> supportedParams = List.of(RADIO_POWER_DBM, COOLING_SYSTEM_POWER_PCT,
            MAIN_ENGINE_THRUST_PCT, ORIENTATION_ZENITH_ANGLE_DEG, ORIENTATION_AZIMUTH_ANGLE_DEG,
            VESSEL_ALTITUDE_M, VESSEL_SPEED_MPS, MAIN_ENGINE_FUEL_PCT, TEMPERATURE_INTERNAL_DEG);

    public static List<String> requiredTelemetryParams = List.of(RADIO_POWER_DBM, COOLING_SYSTEM_POWER_PCT,
            MAIN_ENGINE_THRUST_PCT);

    @Min(20)
    @Max(80)
    private Integer radioPowerDbm;
    @Min(0)
    @Max(100)
    private Integer coolingSystemPowerPct;
    @Min(0)
    @Max(100)
    private Integer mainEngineThrustPct;
    @Min(0)
    @Max(359)
    private Integer orientationZenithAngleDeg;
    @Min(0)
    @Max(359)
    private Integer orientationAzimuthAngleDeg;
    @Min(0)
    @Max(35000000)
    private Integer vesselAltitudeM;
    @Min(0)
    @Max(15000)
    private Integer vesselSpeedMps;
    @Min(0)
    @Max(100)
    private Integer mainEngineFuelPct;
    @Min(-50)
    @Max(50)
    private Integer temperatureInternalDeg;

    public Map<String, String> validate(Collection<String> keys) {
        Map<String, String> errors = ObjectValidator.validate(this);
        Set<String> notSupportedParams = checkNotSupportedParams(keys);
        if (!notSupportedParams.isEmpty()) {
            errors.put(VALIDATION_NOT_SUPPORTED_PARAMS, String.join(",", notSupportedParams));
        }
        return errors;
    }

    public static Set<String> checkNotSupportedParams(Collection<String> keys) {
        return keys.stream()
                .filter(s -> !Settings.supportedParams.contains(s))
                .collect(Collectors.toSet());
    }
}
