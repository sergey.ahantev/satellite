package com.ahantiev.satelite.common.dto;

import lombok.*;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"id"})
public class Operation {
    @Min(1)
    @NotNull
    private Integer id;
    @NotNull
    private Integer deltaT;
    @Min(1)
    @NotNull
    private Integer timeout;
    @NotNull
    private Integer value;
    @NotNull
    private String variable;
    @Builder.Default
    private Boolean critical = true;

    public static Operation.OperationBuilder ofDefault(Integer id, String variable) {
        return Operation.builder().id(id).deltaT(0).timeout(1).value(0).variable(variable);
    }

    public long getTimeoutMillis() {
        return timeout * 1000;
    }
}
