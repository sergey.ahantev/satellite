package com.ahantiev.satelite.management.operation;

import com.ahantiev.satelite.common.dto.Operation;
import com.ahantiev.satelite.common.dto.Settings;
import com.ahantiev.satelite.common.dto.SettingsValue;
import com.ahantiev.satelite.common.time.TimeProvider;
import com.ahantiev.satelite.management.TestApplicationConfiguration;
import com.ahantiev.satelite.management.client.SettingsClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.SocketTimeoutException;
import java.time.Instant;
import java.util.Map;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApplicationConfiguration.class)
public class OperationServiceTest {

    private final static Instant ZERO_TIME = Instant.ofEpochSecond(0);

    @MockBean
    private SettingsClient settingsClient;
    @Autowired
    private OperationService operationService;


    @Test
    public void loadProgram() {

    }


    @Test
    public void executeProgram() {
    }

    @Test
    public void executeOperationOk() {
        TimeProvider.useFixedClockAt(ZERO_TIME);
        Operation operation = Operation.ofDefault(1, Settings.COOLING_SYSTEM_POWER_PCT).build();

        whenUpdateSettingsReturnCorrect(operation);
        whenGetSettingsReturnCorrect(operation);

        ExecutionResult result = operationService.executeOperation(0L, operation);
        assertTrue(result.getErrorMessage(), result.isOk());
        assertFalse(result.isSkipped());
    }

    @Test
    public void executeOperationSkip() {
        TimeProvider.useFixedClockAt(ZERO_TIME.plusSeconds(1L));
        Operation operation = Operation.ofDefault(1, Settings.COOLING_SYSTEM_POWER_PCT).build();

        ExecutionResult result = operationService.executeOperation(0L, operation);
        assertTrue(result.getErrorMessage(), result.isOk());
        assertTrue(result.isSkipped());
    }

    @Test(expected = RuntimeException.class)
    public void executeOperationFailByTimeout() {
        when(settingsClient.updateSettings(any())).thenThrow(new RuntimeException("Timeout error", new SocketTimeoutException()));

        TimeProvider.useFixedClockAt(ZERO_TIME);
        Operation operation = Operation.ofDefault(1, Settings.COOLING_SYSTEM_POWER_PCT).build();

        operationService.executeOperation(0L, operation);
    }

    @Test
    public void executeOperationUpdateSettingsFailed() {
        TimeProvider.useFixedClockAt(ZERO_TIME);
        Operation operation = Operation.ofDefault(1, Settings.COOLING_SYSTEM_POWER_PCT).build();

        when(settingsClient
                .updateSettings(Map.of(operation.getVariable(), operation.getValue())))
                .thenReturn(Map.of(operation.getVariable(), new SettingsValue(100500, null)));

        ExecutionResult result = operationService.executeOperation(0L, operation);
        assertTrue(result.isFail());
        assertEquals(SystemCode.UPDATE_SETTINGS_ERROR, result.getSystemCode());
    }

    @Test
    public void executeOperationCheckSettingsFailed() {
        TimeProvider.useFixedClockAt(ZERO_TIME);
        Operation operation = Operation.ofDefault(1, Settings.COOLING_SYSTEM_POWER_PCT).build();

        whenUpdateSettingsReturnCorrect(operation);
        when(settingsClient
                .getSettings(operation.getVariable()))
                .thenReturn(Map.of(operation.getVariable(), new SettingsValue(null, 100500)));

        ExecutionResult result = operationService.executeOperation(0L, operation);
        assertTrue(result.isFail());
        assertEquals(SystemCode.CHECK_SETTINGS_ERROR, result.getSystemCode());
    }

    private void whenGetSettingsReturnCorrect(Operation operation) {
        when(settingsClient
                .getSettings(operation.getVariable()))
                .thenReturn(Map.of(operation.getVariable(), new SettingsValue(null, operation.getValue())));
    }

    private void whenUpdateSettingsReturnCorrect(Operation operation) {
        when(settingsClient
                .updateSettings(Map.of(operation.getVariable(), operation.getValue())))
                .thenReturn(Map.of(operation.getVariable(), new SettingsValue(operation.getValue(), null)));
    }
}