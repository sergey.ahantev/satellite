package com.ahantiev.satelite.management;

import com.ahantiev.satelite.common.dto.Operation;
import com.ahantiev.satelite.common.time.TimeProvider;
import com.ahantiev.satelite.management.operation.OperationTimer;
import org.junit.Test;

import java.time.Instant;

import static org.junit.Assert.*;

public class OperationTimerTest {
    private final static Instant ZERO_TIME = Instant.ofEpochSecond(0);

    @Test
    public void isActual() {
        TimeProvider.useFixedClockAt(Instant.ofEpochSecond(1554420582));

        Operation operation = Operation.builder().deltaT(10).build();
        OperationTimer operationTimer = new OperationTimer(1554420582, operation);

        assertTrue(operationTimer.isActual());
    }

    @Test
    public void isNotActual() {
        TimeProvider.useFixedClockAt(ZERO_TIME.plusSeconds(21L));

        Operation operation = Operation.ofDefault(1, "").deltaT(10).build();
        OperationTimer operationTimer = new OperationTimer(10, operation);

        assertEquals(20, operationTimer.getStartTimeSec());
        assertEquals(21, operationTimer.getNowSec());
        assertFalse(operationTimer.isActual());
    }

    @Test
    public void getDelayMillis() {
        TimeProvider.useFixedClockAt(ZERO_TIME);

        Operation operation = Operation.builder().deltaT(10).build();
        OperationTimer operationTimer = new OperationTimer(0, operation);

        assertEquals(10_000, operationTimer.getDelayMillis());
    }

    @Test
    public void getTimeToStartSec() {
        TimeProvider.useFixedClockAt(ZERO_TIME);

        Operation operation = Operation.builder().deltaT(10).build();
        OperationTimer operationTimer = new OperationTimer(0L, operation);

        assertEquals(10, operationTimer.getStartTimeSec());

    }
}