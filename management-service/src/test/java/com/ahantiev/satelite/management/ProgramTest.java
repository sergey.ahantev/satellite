package com.ahantiev.satelite.management;

import com.ahantiev.satelite.common.dto.FlightProgram;
import com.ahantiev.satelite.common.dto.Operation;
import com.ahantiev.satelite.common.dto.Settings;
import org.junit.Test;

import java.util.Arrays;
import java.util.Map;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ProgramTest {
    @Test
    public void validateProgram() {
        FlightProgram program1 = FlightProgram.ofDefault(
                Operation.ofDefault(1, Settings.COOLING_SYSTEM_POWER_PCT).build()
        ).build();

        assertTrue(program1.validate().isEmpty());

        FlightProgram program2 = FlightProgram.ofDefault(
                Operation.ofDefault(1, Settings.COOLING_SYSTEM_POWER_PCT).build(),
                Operation.ofDefault(2, Settings.MAIN_ENGINE_FUEL_PCT).build()
        ).build();

        assertTrue(program2.validate().isEmpty());
    }

    @Test
    public void validateProgramFailed() {
        FlightProgram program = FlightProgram.ofDefault(
                Operation.ofDefault(0, Settings.COOLING_SYSTEM_POWER_PCT).build()
        ).build();

        assertFalse(program.validate().isEmpty());
    }

    @Test
    public void operationsIsEmpty() {
        FlightProgram program = FlightProgram.ofDefault().build();

        Map<String, String> errors = program.validate();
        assertFalse(errors.isEmpty());
        assertTrue(errors.containsKey("operations"));
    }

    @Test
    public void startUpIsNull() {
        FlightProgram program = FlightProgram.ofDefault().startUp(null).build();

        Map<String, String> errors = program.validate();
        assertFalse(errors.isEmpty());
        assertTrue(errors.containsKey("startUp"));
    }

    @Test
    public void operationIdNotUnique() {
        FlightProgram program = FlightProgram.ofDefault(
                Operation.ofDefault(1, Settings.COOLING_SYSTEM_POWER_PCT).build(),
                Operation.ofDefault(1, Settings.COOLING_SYSTEM_POWER_PCT).build()
        ).build();

        Map<String, String> errors = program.validate();
        assertFalse(errors.isEmpty());
        assertTrue(errors.containsKey("operation.id"));
    }

    @Test
    public void operationIdInvalid() {
        FlightProgram program = FlightProgram.ofDefault(
                Operation.ofDefault(0, Settings.COOLING_SYSTEM_POWER_PCT).build()
        ).build();

        Map<String, String> errors = program.validate();
        assertFalse(errors.isEmpty());
        assertTrue(errors.containsKey("operations[0].id"));
    }

    @Test
    public void operationVariableNotSupport() {
        FlightProgram program = FlightProgram.ofDefault(
                Operation.ofDefault(1, "UNKNOWN_PARAM").build()
        ).build();

        Map<String, String> errors = program.validate();
        assertFalse(errors.isEmpty());
        assertTrue(errors.containsKey("operations[0].variable"));
    }

    @Test
    public void operationInvalid() {
        FlightProgram program = FlightProgram.ofDefault(
                Operation.builder().build()
        ).build();

        Map<String, String> errors = program.validate();
        assertFalse(errors.isEmpty());
        assertTrue(errors.keySet().containsAll(Arrays.asList(
                "operations[0].id",
                "operations[0].deltaT",
                "operations[0].timeout",
                "operations[0].value",
                "operations[0].variable"
        )));
    }
}