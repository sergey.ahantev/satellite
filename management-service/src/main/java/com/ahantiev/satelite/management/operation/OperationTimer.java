package com.ahantiev.satelite.management.operation;

import com.ahantiev.satelite.common.dto.Operation;
import com.ahantiev.satelite.common.time.TimeProvider;

import java.time.*;
import java.time.temporal.ChronoUnit;

public class OperationTimer {
    private final long startUpSec;
    private final Operation operation;
    private final Instant now;
    private final Instant startTime;

    public OperationTimer(long startUpSec, Operation operation) {
        this.now = TimeProvider.now();
        this.startUpSec = startUpSec;
        this.operation = operation;
        this.startTime = this.calcStartTime();
    }

    private Instant calcStartTime() {
        return Instant.ofEpochSecond(startUpSec)
                .plus(operation.getDeltaT(), ChronoUnit.SECONDS);
    }

    public boolean isActual() {
        return now.isBefore(startTime) || now.equals(startTime);
    }

    public long getDelayMillis() {
        return Duration.between(now, startTime).toMillis();
    }

    public long getNowSec() {
        return now.getEpochSecond();
    }

    public long getStartTimeSec() {
        return startTime.getEpochSecond();
    }
}