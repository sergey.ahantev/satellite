package com.ahantiev.satelite.management;

import com.ahantiev.satelite.management.operation.OperationService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@EnableFeignClients
@SpringBootApplication
@ComponentScan(basePackages = {"com.ahantiev.satelite"})
public class ManagementApp implements CommandLineRunner {

    private final OperationService operationService;

    public ManagementApp(OperationService operationService) {
        this.operationService = operationService;
    }


    @Override
    public void run(String... args) throws Exception {
        operationService.start();
    }

    public static void main(String[] args) throws InterruptedException {
        SpringApplication.run(ManagementApp.class, args);
    }
}
