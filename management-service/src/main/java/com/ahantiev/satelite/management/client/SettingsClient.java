package com.ahantiev.satelite.management.client;

import com.ahantiev.satelite.common.dto.SettingsValue;
import com.ahantiev.satelite.management.config.FeignConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

@FeignClient(value = "settings", url = "${exchangeUrl}", configuration = FeignConfiguration.class)
public interface SettingsClient {
    @PatchMapping(value = "/settings", consumes = MediaType.APPLICATION_JSON_VALUE)
    Map<String, SettingsValue> updateSettings(@RequestBody Map<String, Object> body);

    @GetMapping(value = "/settings/{keys}", consumes = MediaType.APPLICATION_JSON_VALUE)
    Map<String, SettingsValue> getSettings(@PathVariable String... keys);
}
