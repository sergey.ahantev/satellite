package com.ahantiev.satelite.management.config;

import feign.Request;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.ConfigurableEnvironment;

@Configuration
public class FeignConfiguration {

    @Value("${connectTimeoutMillis:100}")
    private int connectTimeoutMillis;

    @Bean
    public Request.Options requestOptions(ConfigurableEnvironment env) {
        return new Request.Options(connectTimeoutMillis, connectTimeoutMillis);
    }
}