package com.ahantiev.satelite.management.operation;

import feign.FeignException;
import lombok.Data;

@Data
public class ExecutionResult {

    private String errorMessage;
    private SystemCode systemCode;
    private boolean skipped;

    private ExecutionResult() {
    }

    private ExecutionResult(String errorMessage, SystemCode systemCode) {
        this.errorMessage = errorMessage;
        this.systemCode = systemCode;
    }

    private ExecutionResult(boolean skipped) {
        this.skipped = skipped;
    }

    public static ExecutionResult ok() {
        return new ExecutionResult();
    }

    public static ExecutionResult skip() {
        return new ExecutionResult(true);
    }

    public static ExecutionResult fail(String errorMessage, SystemCode systemCode) {
        return new ExecutionResult(errorMessage, systemCode);
    }

    public static ExecutionResult fail(Throwable e) {
        if (e instanceof FeignException) {
            return ExecutionResult.fail(e.getMessage(), SystemCode.INVALID_REQUEST);
        } else {
            return ExecutionResult.fail(e.getMessage(), SystemCode.UNKNOWN_ERROR);
        }
    }

    public boolean isOk() {
        return systemCode == null;
    }

    public boolean isFail() {
        return systemCode != null;
    }

    public boolean isSkipped() {
        return skipped;
    }
}
