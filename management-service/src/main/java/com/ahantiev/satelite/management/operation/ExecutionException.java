package com.ahantiev.satelite.management.operation;

public class ExecutionException extends RuntimeException {
    private SystemCode systemCode;

    public ExecutionException(SystemCode systemCode) {
        this.systemCode = systemCode;
    }

    public ExecutionException(String message, SystemCode systemCode) {
        super(message);
        this.systemCode = systemCode;
    }

    public int getSystemCode() {
        return systemCode.exitCode;
    }
}
