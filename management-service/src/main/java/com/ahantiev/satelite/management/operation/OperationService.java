package com.ahantiev.satelite.management.operation;

import com.ahantiev.satelite.common.dto.FlightProgram;
import com.ahantiev.satelite.common.dto.Operation;
import com.ahantiev.satelite.common.dto.SettingsValue;
import com.ahantiev.satelite.management.client.SettingsClient;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.ahantiev.satelite.common.log.CustomJsonLayout.MDC_PARAM_TYPE;

@Service
public class OperationService {

    private final static Logger log = LoggerFactory.getLogger("jsonLogger");
    private final ObjectMapper objectMapper;
    private final SettingsClient settingsClient;
    private final TelemetryService telemetryService;

    @Value("${flightProgram}")
    private String flightProgramPath;

    @Autowired
    public OperationService(ObjectMapper objectMapper, SettingsClient settingsClient,
                            TelemetryService telemetryService) {
        this.objectMapper = objectMapper;
        this.settingsClient = settingsClient;
        this.telemetryService = telemetryService;
    }

    public void start() {
        try {
            FlightProgram program = loadProgram(flightProgramPath);
            validate(program);
            telemetryService.start();
            executeProgram(program);
            log.info("Program exit code: {}", SystemCode.OK.exitCode);
            System.exit(SystemCode.OK.exitCode);
        } catch (ExecutionException e) {
            log.info("Program exit code: {}", e.getSystemCode());
            System.exit(e.getSystemCode());
        }
    }

    private void validate(FlightProgram program) throws ExecutionException {
        log.info("Validation program started");
        Map<String, String> errors = program.validate();
        if (!errors.isEmpty()) {
            log.error("Validation errors count {}. {}", errors.size(), errors.toString());
            throw new ExecutionException(SystemCode.INVALID_PROGRAM_ERROR);
        }
        log.info("Validation program successfully completed!");
    }


    public FlightProgram loadProgram(String path) throws ExecutionException {
        log.info("Loading program from path: {}", path);

        String flightProgramJson;
        try {
            flightProgramJson = Files.readString(Paths.get(path), StandardCharsets.UTF_8);
        } catch (IOException e) {
            log.error("Loading program failed! File not found");
            throw new ExecutionException(SystemCode.INVALID_PROGRAM_ERROR);
        }

        FlightProgram flightProgram;
        try {
            flightProgram = objectMapper.readValue(flightProgramJson, FlightProgram.class);
        } catch (JsonMappingException e) {
            log.error("Loading program failed! Incorrect json file. {}", e.getLocation());
            throw new ExecutionException(SystemCode.INVALID_PROGRAM_ERROR);
        } catch (IOException e) {
            log.error("Loading program failed! Incorrect json file. {}", e.getMessage());
            throw new ExecutionException(SystemCode.INVALID_PROGRAM_ERROR);
        }

        return flightProgram;
    }


    public void executeProgram(FlightProgram program) throws ExecutionException {
        MDC.put(MDC_PARAM_TYPE, "error");

        ExecutionResult critical = null;
        List<ExecutionResult> results = new ArrayList<>();

        for (Operation operation : program.getOperations()) {
            if (telemetryService.isFailed()) {
                break;
            }

            ExecutionResult result;
            try {
                result = executeOperation(program.getStartUp(), operation);
            } catch (Throwable e) {
                result = ExecutionResult.fail(e);
            }

            results.add(result);

            if (result.isFail()) {
                log.error("Operation failed! Operation: {} cause: {}, code: {}, critical: {}", operation.getId(),
                        result.getErrorMessage(), result.getSystemCode().exitCode, operation.getCritical());

                if (operation.getCritical()) {
                    critical = result;
                    break;
                }
            }
        }

        String statistic = getStatistic(results);

        if (telemetryService.isFailed()) {
            ExecutionResult result = telemetryService.getFailResult();
            log.error("Program failed! code: {}, statistic: {}, cause: {}",
                    result.getSystemCode().exitCode, statistic, result.getErrorMessage());
            throw new ExecutionException(result.getErrorMessage(), result.getSystemCode());
        } else if (critical != null) {
            log.error("Program failed! code: {}, statistic: {}, cause: {}",
                    critical.getSystemCode().exitCode, statistic, critical.getErrorMessage());
            throw new ExecutionException(critical.getErrorMessage(), critical.getSystemCode());
        } else {
            log.info("Program successfully completed! statistic: {}", statistic);
        }
    }

    public ExecutionResult executeOperation(long startUpSec, Operation operation) throws ExecutionException {
        log.info("Operation: {} started!", operation.getId());

        OperationTimer operationTimer = new OperationTimer(startUpSec, operation);
        if (!operationTimer.isActual()) {
            log.info("Operation: {} skipped. Time(sec) now: {} > startTime: {}", operation.getId(),
                    operationTimer.getNowSec(), operationTimer.getStartTimeSec());
            return ExecutionResult.skip();
        }

        log.info("Operation: {} paused: {} millis", operation.getId(), operationTimer.getDelayMillis());
        setTimeout(operationTimer.getDelayMillis());

        log.info("Operation: {} try update variable {} to {}", operation.getId(), operation.getVariable(),
                operation.getValue());
        Map<String, SettingsValue> updateResult = settingsClient.updateSettings(Map.of(operation.getVariable(),
                operation.getValue()));

        SettingsValue updatedValue = updateResult.get(operation.getVariable());
        if (updatedValue == null || !operation.getValue().equals(updatedValue.getSet())) {
            return ExecutionResult.fail("Failed to set values for " + operation.getVariable(),
                    SystemCode.UPDATE_SETTINGS_ERROR);
        }

        log.info("Operation: {} paused: {} seconds", operation.getId(), operation.getTimeout());
        setTimeout(operation.getTimeoutMillis());

        log.info("Operation: {} try check variable: {}", operation.getId(), operation.getVariable());
        Map<String, SettingsValue> settings = settingsClient.getSettings(operation.getVariable());
        SettingsValue value = settings.get(operation.getVariable());

        if (!operation.getValue().equals(value.getValue())) {
            log.error("Operation: {} invalid value. Expected: {} actual: {}!", operation.getId(),
                    operation.getValue(), value.getValue());
            return ExecutionResult.fail("Failed to check values for" + operation.getVariable(),
                    SystemCode.CHECK_SETTINGS_ERROR);
        }
        log.info("Operation: {} completed!", operation.getId());

        return ExecutionResult.ok();
    }

    private void setTimeout(long timeoutMillis) {
        try {
            TimeUnit.MILLISECONDS.sleep(timeoutMillis);
        } catch (InterruptedException e) {
            throw new ExecutionException(e.getMessage(), SystemCode.UNKNOWN_ERROR);
        }
    }

    private String getStatistic(List<ExecutionResult> results) {
        int successful = 0;
        int failed = 0;
        int skipped = 0;
        for (ExecutionResult result : results) {
            if (result.isOk()) successful++;
            if (result.isFail()) failed++;
            if (result.isSkipped()) skipped++;
        }
        return String.format("successful: %d (skipped: %d), failed: %d", successful, skipped, failed);
    }
}
