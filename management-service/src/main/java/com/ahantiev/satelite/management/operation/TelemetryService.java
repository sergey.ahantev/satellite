package com.ahantiev.satelite.management.operation;

import com.ahantiev.satelite.common.dto.Settings;
import com.ahantiev.satelite.common.dto.SettingsValue;
import com.ahantiev.satelite.management.client.SettingsClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static com.ahantiev.satelite.common.log.CustomJsonLayout.MDC_PARAM_TYPE;

@Service
public class TelemetryService {

    private final static Logger log = LoggerFactory.getLogger("jsonLogger");
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    private final AtomicReference<ExecutionResult> telemetrySendingFail = new AtomicReference<>();
    private final SettingsClient settingsClient;

    @Value("${telemetryFreq}")
    private Long telemetryFreqSec;

    public TelemetryService(SettingsClient settingsClient) {
        this.settingsClient = settingsClient;
    }

    public boolean isFailed() {
        return telemetrySendingFail.get() != null;
    }

    public ExecutionResult getFailResult() {
        return telemetrySendingFail.get();
    }

    public void start() {
        this.sendTelemetry();
        scheduler.scheduleAtFixedRate(this::sendTelemetry, telemetryFreqSec, telemetryFreqSec,
                TimeUnit.SECONDS);
    }

    private void sendTelemetry() {
        if (telemetrySendingFail.get() != null) {
            return;
        }
        MDC.put(MDC_PARAM_TYPE, "values");
        try {
            Map<String, SettingsValue> values = settingsClient.getSettings(
                    Settings.requiredTelemetryParams.toArray(new String[0]));
            String params = values.entrySet().stream()
                    .map(entry -> entry.getKey() + "=" + entry.getValue().value)
                    .collect(Collectors.joining("&"));
            log.error(params);
        } catch (Throwable e) {
            ExecutionResult fail = ExecutionResult.fail(e);
            telemetrySendingFail.set(fail);
            log.error("Send telemetry failed! code: {}. Cause: {}", fail.getSystemCode().exitCode,
                    fail.getErrorMessage());
        } finally {
            MDC.clear();
        }
    }
}
