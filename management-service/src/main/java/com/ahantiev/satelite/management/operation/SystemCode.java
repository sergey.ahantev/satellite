package com.ahantiev.satelite.management.operation;

public enum SystemCode {
    OK(0),
    INVALID_PROGRAM_ERROR(1),
    UPDATE_SETTINGS_ERROR(11),
    INVALID_REQUEST(11),
    CHECK_SETTINGS_ERROR(12),
    UNKNOWN_ERROR(13);

    public final int exitCode;

    SystemCode(int exitCode) {
        this.exitCode = exitCode;
    }
}