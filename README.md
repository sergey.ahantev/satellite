# Project Satellite

# Modules:

* **common** - модуль общих зависимостей (dto, utils) 
* **communication-service** - REST API коммуникации с внешними модулями
* **management-service** - сервис управления внешними модулями

# Architecture

* **communication-service** является Spring Boot Web приложением с встроенным web-сервером. Сервис содержит внутри себя In-memory хранилище для хранения параметров других модулей системы,предоставляет эндпоинты для установки и чтения параметров. Сервис запускается в одном экземпляре
* **management-service** является консольным приложением Spring Boot. Общается с **communication-service** по протоколу HTTP. Может запускаться в нескольких экземплярах

# Dependencies

* **Spring Boot** - представляет оберкту над Spring Framework, упрощает работу с зависимостями и конфигурацией приложения посредством starter-пакетов
* **spring-boot-starter-web** - Spring MVC + embeded web server
* **spring-boot-starter-validation** - удобная виладация с помошью аннотаций
* **spring-cloud-starter-openfeign** - обертка над RestTemplate, позволяет в декларативном стиле описывать Rest Client
* **spring-boot-starter-test(JUnit, Mockito, MockMvc)** - тесты
* **logback**- гибкая, модульная система логирования
* **jackson** - маппинга json в pojo и наоборот
* **lombok** - уменьшение бойлерплейта посредством генерации кода

# Clone

> git clone https://gitlab.com/sergey.ahantev/satellite.git

# Setup
> Для запуска приложения необходимо иметь: **JDK 11**

> Поменять настройки **/project/dir/management-service/src/main/resources/application.yml**

```
flightProgram: 'pathToFile'
telemetryFreq: 10
exchangeUrl: 'http://localhost:8080/api/v1'
```

# Run
```
cd /project/dir
```
**Run Tests**

```
./mvnw test
```

**Build**

```
./mvnw install
```

**Run app**

```
./mvnw spring-boot:run -pl communication-service
./mvnw spring-boot:run -pl management-service
```








