package com.ahantiev.satelite.communication;

import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.util.Map;

@Getter
public class ValidationException extends RuntimeException {
    private final Map<String, String> errors;

    public ValidationException(Map<String, String> errors) {
        this.errors = errors;
    }

    @Override
    public String getMessage() {
        return "Validation error: " + errors;
    }
}
