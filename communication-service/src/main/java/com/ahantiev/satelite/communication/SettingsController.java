package com.ahantiev.satelite.communication;

import com.ahantiev.satelite.common.dto.Settings;
import com.ahantiev.satelite.common.dto.SettingsValue;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;
import java.util.Set;

@Slf4j
@RestController
@ControllerAdvice
@RequestMapping("/api/v1")
public class SettingsController {

    private final SettingsService settingsService;
    private final ObjectMapper objectMapper;

    @Autowired
    public SettingsController(SettingsService settingsService, ObjectMapper objectMapper) {
        this.settingsService = settingsService;
        this.objectMapper = objectMapper;
    }

    @GetMapping(value = "/settings/{keys}")
    public Map<String, SettingsValue> getSettings(@PathVariable String... keys) {
        Set<String> notSupportedParams = Settings.checkNotSupportedParams(Arrays.asList(keys));
        if (!notSupportedParams.isEmpty()) {
            throw new ValidationException(Map.of(Settings.VALIDATION_NOT_SUPPORTED_PARAMS,
                    String.join(",", notSupportedParams)));
        }
        return settingsService.getSettings(keys);
    }

    @PatchMapping(value = "/settings", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, SettingsValue> updateSettings(@RequestBody Map<String, Object> body) {
        Settings settings = objectMapper.convertValue(body, Settings.class);

        Map<String, String> errors = settings.validate(body.keySet());
        if (!errors.isEmpty()) {
            throw new ValidationException(errors);
        }
        return settingsService.updateSettings(body);
    }

    @ExceptionHandler(value = ValidationException.class)
    public ResponseEntity<Object> exception(ValidationException exception) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .contentType(MediaType.APPLICATION_JSON)
                .contentLength(exception.getMessage().length())
                .body(exception.getMessage());
    }

}
