package com.ahantiev.satelite.communication;

import com.ahantiev.satelite.common.dto.SettingsValue;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

@Service
public class SettingsService {

    private final Map<String, Object> settingsStore = new ConcurrentHashMap<>();
    private final ReadWriteLock lock = new ReentrantReadWriteLock();

    public Map<String, SettingsValue> updateSettings(Map<String, Object> settings) {
        lock.writeLock().lock();
        try {
            Map<String, SettingsValue> values = new HashMap<>();

            for (Map.Entry<String, Object> entry : settings.entrySet()) {
                if (entry.getValue() == null) continue;

                Object currentValue = settingsStore.put(entry.getKey(), entry.getValue());
                values.put(entry.getKey(), new SettingsValue(entry.getValue(), currentValue));
            }
            return values;
        } finally {
            lock.writeLock().unlock();
        }
    }

    public Map<String, SettingsValue> getSettings(String... params) {
        lock.readLock().lock();
        try {

            Map<String, SettingsValue> values = new HashMap<>();
            for (String param : params) {
                values.put(param, new SettingsValue(null, settingsStore.get(param)));
            }
            return values;
        } finally {
            lock.readLock().unlock();
        }
    }
}
