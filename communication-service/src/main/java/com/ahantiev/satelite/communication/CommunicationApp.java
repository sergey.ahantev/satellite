package com.ahantiev.satelite.communication;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.web.client.RestTemplate;

@Slf4j
@SpringBootApplication
public class CommunicationApp {

    public static void main(String[] args) {
        SpringApplication.run(CommunicationApp.class, args);
    }

}
