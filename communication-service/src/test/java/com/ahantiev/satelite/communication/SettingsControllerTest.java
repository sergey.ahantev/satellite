package com.ahantiev.satelite.communication;

import com.ahantiev.satelite.common.dto.Settings;
import com.ahantiev.satelite.common.dto.SettingsValue;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class SettingsControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void updateSettings() throws Exception {
        Settings settings = Settings.builder()
                .radioPowerDbm(20)
                .coolingSystemPowerPct(1)
                .mainEngineThrustPct(1)
                .build();

        mockMvc.perform(patch("/api/v1/settings")
                .content(toJson(settings))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.radioPowerDbm").value(new SettingsValue(20, null)))
                .andExpect(jsonPath("$.coolingSystemPowerPct").value(new SettingsValue(1, null)))
                .andExpect(jsonPath("$.mainEngineThrustPct").value(new SettingsValue(1, null)))
        ;

        Settings settings2 = Settings.builder()
                .radioPowerDbm(21)
                .coolingSystemPowerPct(2)
                .mainEngineThrustPct(2)
                .build();

        mockMvc.perform(patch("/api/v1/settings")
                .content(toJson(settings2))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.radioPowerDbm").value(new SettingsValue(21, 20)))
                .andExpect(jsonPath("$.coolingSystemPowerPct").value(new SettingsValue(2, 1)))
                .andExpect(jsonPath("$.mainEngineThrustPct").value(new SettingsValue(2, 1)))
        ;
    }

    @Test
    public void updateSettingsValidationFailed() throws Exception {
        Settings settings = Settings.builder()
                .radioPowerDbm(100500)
                .coolingSystemPowerPct(1)
                .mainEngineThrustPct(1)
                .build();

        mockMvc.perform(patch("/api/v1/settings")
                .content(toJson(settings))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
        ;
    }

    @Test
    public void getSettings() throws Exception {
        Settings settings = Settings.builder()
                .orientationZenithAngleDeg(359)
                .orientationAzimuthAngleDeg(359)
                .build();

        mockMvc.perform(patch("/api/v1/settings")
                .content(toJson(settings))
                .contentType(MediaType.APPLICATION_JSON));

        mockMvc.perform(get("/api/v1/settings/orientationZenithAngleDeg,orientationAzimuthAngleDeg")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.orientationZenithAngleDeg").value(new SettingsValue(null, 359)))
                .andExpect(jsonPath("$.orientationAzimuthAngleDeg").value(new SettingsValue(null, 359)))
        ;
    }

    @Test
    public void getSettingsFailed() throws Exception {

        mockMvc.perform(get("/api/v1/settings/UNKNOWN"))
                .andExpect(status().is4xxClientError())
        ;
    }

    private String toJson(Object settingsRequest) throws JsonProcessingException {
        return objectMapper.writeValueAsString(settingsRequest);
    }

}